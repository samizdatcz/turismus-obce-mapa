function getColor(before, after) {
  var val = after / before

  var col;
  if (val <= 0.573732) { col = '#b2182b' } else
  if (val <= 0.858482) { col = '#ef8a62' } else
  if (val <= 1) { col = '#fddbc7' } else
  if (val <= 1.297014) { col = '#d1e5f0' } else
  if (val <= 1.739106) { col = '#67a9cf' } else
    if (val > 1.739106) { col = '#2166ac' } else
    {col = "white"}

  var style = new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: "lightgray",
      width: 0.7
    }),
    fill: new ol.style.Fill({
      color: col
    })
  })
  return style;
};

function niceDate(date) {
  var s = String(date)
  return parseInt(s.slice(4)) + '. ' + parseInt(s.slice(0, 4))
}

function makeTooltip(evt) {
 document.getElementById('tooltip').innerHTML = '<div id="chart"></div>'

var data = {};
for (var key in evt) {
  if (key.startsWith('r_')) {
    data[parseInt(key.replace('r_', ''))] = evt[key]
  }
}

var rozdil = Math.round(((data[2016] / data[2012]) - 1) * 1000) / 10;

var blabol;
if (isNaN(rozdil)){
  document.getElementById('chart').innerHTML = '<b>' + evt.NAZ_ORP + '</b>, ' + evt.NAZ_CZNUTS3 + ' (' + evt.POCET_OBYV + ' obyv.)<br>Pro oblast chybí data.'
  return;
}
else if (rozdil >= 0) {
  blabol = '<span style="color: #0571b0;">přibylo ' + Math.abs(rozdil) + ' % turistů</span>'
} else {
  blabol = '<span style="color: #b2182b;">ubylo ' + Math.abs(rozdil) + ' % turistů</span>'
};

Highcharts.chart('chart', {
   chart: {
     height: 200,
     //width: window.innerWidth / 4,
     type: 'column'
   },
   title: {
       text: null
   },
   subtitle: {
     align: 'left',
     useHTML: true,
     text: '<b>' + evt.NAZ_ORP + '</b>, ' + evt.NAZ_CZNUTS3 + ' (' + evt.POCET_OBYV + ' obyv.)<br>Mezi léty 2012 a 2016 <b>' + blabol  + '</b>' 
   },
   xAxis: {
       categories: Object.keys(data).map(function(v) {return v}),
       crosshair: true
   },
   yAxis: {
       min: 0,
       title: {
           text: 'Počet turistů'
       }
   },
   credits: { enabled: false },
   tooltip: { enabled: false },
   legend: { enabled: false },
   plotOptions: {
       column: {
         animation: false,
           pointPadding: 0.2,
           borderWidth: 0
       }
   },
   series: [{
       name: 'turisti',
       data: Object.values(data)

   }]
});
};

var tilegrid = ol.tilegrid.createXYZ({tileSize: 512, maxZoom: 11});

var background = new ol.layer.Tile({
  source: new ol.source.OSM({
    url: 'https://interaktivni.rozhlas.cz/tiles/ton_b1/{z}/{x}/{y}.png',
    attributions: [
      new ol.Attribution({ html: 'obrazový podkres <a target="_blank" href="http://stamen.com">Stamen</a>, <a target="_blank" href="https://samizdat.cz">Samizdat</a>, data <a target="_blank" href="https://www.czso.cz/csu/czso/uchazeci-o-zamestnani-dosazitelni-a-podil-nezamestnanych-osob-podle-obci">ČSÚ</a>'})
    ]
  })
})

var labels = new ol.layer.Tile({
  source: new ol.source.OSM({
    url: 'https://interaktivni.rozhlas.cz/tiles/ton_l1/{z}/{x}/{y}.png'
  })
})

var layer = new ol.layer.VectorTile({
  source: new ol.source.VectorTile({
    format: new ol.format.MVT(),
    tileGrid: tilegrid,
    tilePixelRatio: 8,
    url: 'https://interaktivni.rozhlas.cz/data/turismus-obce-mapa/tiles/{z}/{x}/{y}.pbf'
  }),
  style: function(feature) {
    return getColor(feature.get('r_2012'), feature.get('r_2016'))
  }
});

var initZoom;

if (window.innerWidth < 768) {
  initZoom = 6;
  document.getElementById('tooltip').innerHTML = 'Kliknutím vyberte obec.'
} else {
  initZoom = 8;
}

var map = new ol.Map({
  interactions: ol.interaction.defaults({mouseWheelZoom:false}),
  target: 'map',
  view: new ol.View({
    center: ol.proj.transform([15.3350758, 49.7417517], 'EPSG:4326', 'EPSG:3857'), //Číhošť
    zoom: initZoom,
    minZoom: 6,
    maxZoom: 9
  })
});

map.addLayer(background);
map.addLayer(layer);
map.addLayer(labels);

map.on('pointermove', function(evt) {
  if (evt.dragging) {
    return;
  }
  var pixel = map.getEventPixel(evt.originalEvent);
  if (map.hasFeatureAtPixel(pixel)){
    map.forEachFeatureAtPixel(pixel, function(feature, layer) {
      makeTooltip(feature.c);
    });
  } else {
    document.getElementById('tooltip').innerHTML = 'Najetím vyberte obec.'
  }
});

//mobil
map.on('singleclick', function(evt) {
  var pixel = map.getEventPixel(evt.originalEvent);
  if (map.hasFeatureAtPixel(pixel)){
    map.forEachFeatureAtPixel(pixel, function(feature, layer) {
      makeTooltip(feature.c);
    });
  } else {
    document.getElementById('tooltip').innerHTML = 'Kliknutím vyberte obec.'
  }
});